<!DOCTYPE HTML>
<html>
<head>
    <meta name="csrf-token" content="{{csrf_token()}}">


    <title>Пример веб-страницы</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

</head>
<body>
<div class="container">
    @if($errors->any())
            @foreach($errors->all() as $error)
                <div class="alert alert-danger alert-dismissible fade show " role="alert">
                    {{$error}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="close">
                        <span aria-hidden="true">x</span>
                    </button>
                </div>
            @endforeach
    @endif
        @if(session('success'))


            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    <div id="app">


        <form action="{{route('form.store')}}" method="post" >@csrf
            <form-component></form-component>
        </form>



    </div>

</div>



<script src="{{asset('js/app.js')}}" defer></script>
</body>
</html>

