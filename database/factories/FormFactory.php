<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Form;
use Faker\Generator as Faker;

$factory->define(Form::class, function (Faker $faker) {
    $name = $faker->name;
    $descr = $faker->realText(rand(10,100));

    return [
        'name'=>$name,
        'phone'=>$faker->phoneNumber,
        'descr'=>$descr,

    ];
});

